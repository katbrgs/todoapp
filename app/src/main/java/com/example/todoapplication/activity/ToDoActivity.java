package com.example.todoapplication.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.todoapplication.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addTaskAction();
    }

    private void addTaskAction() {
        FloatingActionButton btnShow = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        btnShow.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        TextView tvEditText = (TextView) findViewById(R.id.editTextTextMultiLine);
        TextView tvName = (TextView) findViewById(R.id.textView);
        tvEditText.setText(tvName.getText().toString());
        tvName.setText("");
    }
}