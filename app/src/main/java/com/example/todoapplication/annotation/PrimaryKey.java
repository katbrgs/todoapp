package com.example.todoapplication;

public @interface PrimaryKey {
    boolean autoGenerate();
}
