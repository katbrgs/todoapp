package com.example.todoapplication;

public @interface Entity {
    String tableName();
}
