package com.example.todoapplication;

import com.example.todoapplication.annotation.Entity;
import com.example.todoapplication.annotation.PrimaryKey;

@Entity(tableName = "todo_table")
public class ToDoModel {
    @PrimaryKey(autoGenerate = true)

    public ToDoModel(String toDo) {
        this.toDo = toDo;
    }

    private int id;

    private String toDo;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToDo() {
        return toDo;
    }

    public void setToDo(String toDo) {
        this.toDo = toDo;
    }

}
